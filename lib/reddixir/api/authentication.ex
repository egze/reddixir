defmodule Reddixir.API.Authentication do

  @moduledoc """
  Provides an interface for acquiring access tokens. Requesting a token
  is considered a special case and is NOT subject to rate limiting through
  the priority queue.
  """

  alias Reddixir.Config
  alias Reddixir.Request
  alias Reddixir.Parser

  @token_endpoint "https://www.reddit.com/api/v1/access_token"


  @doc """
  Grants the client an access token.
  """

  def request_token do
    creds = Config.credentials
    body = [grant_type: "password", username: creds[:username], password: creds[:password]]
    basic_auth = Mojito.Headers.auth_header(creds[:client_id], creds[:client_secret])
    retry_until_success(@token_endpoint, body, basic_auth)
  end

  defp retry_until_success(endpoint, body, basic_auth) do
    endpoint
    |> Request.request_token(body, basic_auth)
    |> parse_response_or_retry(endpoint, body, basic_auth)
  end
  
  defp parse_response_or_retry({{:ok, response}, strategy}, endpoint, body, basic_auth) do
    case response.status_code do
      c when c in 500..599 -> retry_until_success(endpoint, body, basic_auth)
      _ -> Parser.parse(response, strategy)
    end
  end
  
  defp parse_response_or_retry(_, endpoint, body, basic_auth) do
    retry_until_success(endpoint, body, basic_auth)
  end

end