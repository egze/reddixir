defmodule Reddixir.Request do

  alias Reddixir.TokenServer
  alias Reddixir.Config

  @moduledoc """
  A thin wrapper around Mojito's HTTP request function. All requests are processed inside
  of this module after being received from the request server.
  """

  def request_token(url, query, basic_auth) do
    options = [method: :post, url: url, body: encode_post_query(query), headers: [basic_auth] ++ headers()]

    {Mojito.request(options), :token}
  end

  def request(:get, url, parse_strategy) do
    options = [method: :get, url: url, headers: headers(:token_required)]
      
    {Mojito.request(options), parse_strategy}
  end

  def request(:get, url, query, parse_strategy) do
    options = [method: :get, url: encode_get_query(url, query), headers: headers(:token_required)]
  
    {Mojito.request(options), parse_strategy}
  end

  def request(:post, url, query, parse_strategy) do
    options = [method: :post, url: url, body: encode_post_query(query), headers: headers(:token_required)]

    {Mojito.request(options), parse_strategy}
  end

  defp headers(:token_required) do
    [{"Authorization", TokenServer.token()}] ++ headers()
  end

  defp headers do
    [
      {"content-type", "application/x-www-form-urlencoded"},
      {"user-agent", Config.user_agent()}
    ]
  end

  defp encode_get_query(url, query) do
    url
    |> URI.parse()
    |> Map.put(:query, URI.encode_query(query))
    |> URI.to_string()
  end

  defp encode_post_query(query) do
    URI.encode_query(query)
  end

end