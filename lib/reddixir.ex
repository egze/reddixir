defmodule Reddixir do
  @moduledoc """
  Documentation for `Reddixir`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Reddixir.hello()
      :world

  """
  def hello do
    :world
  end
end
